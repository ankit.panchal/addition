# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.0.4](https://gitlab.com/ankit.panchal/addition/compare/v3.0.3...v3.0.4) (2023-05-17)

**Note:** Version bump only for package addition-ankit-new





## [3.0.3](https://gitlab.com/ankit.panchal/addition/compare/v3.0.2...v3.0.3) (2023-05-11)

**Note:** Version bump only for package addition-ankit





## [3.0.2](https://gitlab.com/ankit.panchal/addition/compare/v3.0.1...v3.0.2) (2023-05-11)

**Note:** Version bump only for package addition-ankit-new





## [3.0.1](https://gitlab.com/ankit.panchal/addition/compare/v3.0.0...v3.0.1) (2023-05-11)


* fix(package) : add ([6193afd](https://gitlab.com/ankit.panchal/addition/commit/6193afdda1ccceb3b44e872989e936198ae526d5))


### BREAKING CHANGES

* wont work





# [3.0.0](https://gitlab.com/ankit.panchal/addition/compare/v2.2.1...v3.0.0) (2023-05-11)


### Features

* **core:** change ([e90fdd3](https://gitlab.com/ankit.panchal/addition/commit/e90fdd38b518ff9c8bcef79d00d2e38793c06bcd))


### BREAKING CHANGES

* **core:** It will not work





## [2.2.1](https://gitlab.com/ankit.panchal/addition/compare/v2.2.0...v2.2.1) (2023-05-11)

**Note:** Version bump only for package addition-ankit-new





# [2.2.0](https://gitlab.com/ankit.panchal/addition/compare/v2.1.10...v2.2.0) (2023-05-11)


### Features

* **pencil:** add 'graphiteWidth' option ([a9e0444](https://gitlab.com/ankit.panchal/addition/commit/a9e044479426d31b64eaf60ece4ee9508c7274db))





## [2.1.10](https://gitlab.com/ankit.panchal/addition/compare/v2.1.9...v2.1.10) (2023-05-11)

**Note:** Version bump only for package addition-ankit-new





## [2.1.9](https://gitlab.com/ankit.panchal/addition/compare/v2.1.8...v2.1.9) (2023-05-11)

**Note:** Version bump only for package addition-ankit-new





## [2.1.8](https://gitlab.com/ankit.panchal/addition/compare/v2.1.7...v2.1.8) (2023-05-11)


### Bug Fixes

* tag ([3ccef9d](https://gitlab.com/ankit.panchal/addition/commit/3ccef9d1a8f18ee43c25b0dbef35f7b554871246))





## [2.1.7](https://gitlab.com/ankit.panchal/addition/compare/v2.1.6...v2.1.7) (2023-05-11)


### Features

* break ([8ca8786](https://gitlab.com/ankit.panchal/addition/commit/8ca878692a371eff8ddf8fc11f26449349cb13b9))





## [2.1.6](https://gitlab.com/ankit.panchal/addition/compare/v2.1.5...v2.1.6) (2023-05-11)

**Note:** Version bump only for package addition-ankit-new





## [2.1.5](https://gitlab.com/ankit.panchal/addition/compare/v2.1.4...v2.1.5) (2023-05-11)

**Note:** Version bump only for package addition-ankit-new





## [2.1.4](https://gitlab.com/ankit.panchal/addition/compare/v2.1.3...v2.1.4) (2023-05-11)

**Note:** Version bump only for package addition-ankit-new





## [2.1.3](https://gitlab.com/ankit.panchal/addition/compare/v2.1.2...v2.1.3) (2023-05-11)


### Bug Fixes

* Add username ([4852417](https://gitlab.com/ankit.panchal/addition/commit/48524172f7603f054d985b9c8996c850b0a1919e))
* adding GL Token ([a4b7665](https://gitlab.com/ankit.panchal/addition/commit/a4b7665890f4edf1b775cb590a7b507d16dafbb1))
* branch name ([0f7b423](https://gitlab.com/ankit.panchal/addition/commit/0f7b4236288d324d6f6976d501662426bdbaa8f0))
* change ([86b61a9](https://gitlab.com/ankit.panchal/addition/commit/86b61a9239af44942407520b52212cbb93644f4f))
* minor ([53c2a45](https://gitlab.com/ankit.panchal/addition/commit/53c2a45aab3c408405f9f7fe7421f6450199ae5e))
